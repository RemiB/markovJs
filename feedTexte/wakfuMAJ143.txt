Découvrez ci-dessous les contenus qui seront apportés en jeu lors de la mise à jour du 7 juillet.
 
COMBAT / SORTS
 
Limites PA et PM
 
Elles passent à 14 PA et 8 PM, au lieu de 12 PA et 7 PM
 
Tacle et Esquive
 
Les pertes de PA et PM sont désormais affichées dans le chat
Les pertes de PA et PM sont mieux calculées et prises en compte pour la gestion de certains effets
 
Parade

La Parade réduit désormais les coups de 20% (au lieu de 30%). Cette modification a deux raisons :
 
Cela correspond à l’inverse des coups critiques qui augmentent quant à eux les dégâts de 25% (1,25x0,8 = 1)
Certains passifs sur les personnages de type « tank » pourront augmenter le montant de dégâts réduits lors d’une Parade. La caractéristique Parade devient ainsi davantage l’apanage des « tanks ».
 
Résistance aux soins
 
La résistance aux soins montera désormais plus lentement. Auparavant, 100% de résistance aux soins apparaissait lorsqu’un personnage avait été soigné de 3 fois sa vie. Désormais, ce montant est passé à 5. Résumé plus simplement, la résistance aux soins augmente de 67% mais de manière moins rapide dans le temps.
 
Enflammé
 
L’état est désormais remplacé quand réappliqué, et non plus cumulable
Les valeurs d’Enflammé appliquées par les classes sont rééquilibrées en conséquence
L’état Enflammé ne dure qu’un tour
Les cibles ne nécessitent plus d’être échaudées pour subir des dégâts
 
Échaudé
 
Les dégâts d'Enflammé sont augmentés de 20% sur le porteur de l’état Échaudé
 
Armure
 
Le montant d’armure sera désormais affiché dans le cœur des personnages
Au survol du cœur, il est possible de voir le montant exact d’armure possédé
Avec un clic droit sur un personnage, il est possible de savoir combien d’Armure celui-ci possède
 
Ancrage (Bonus de Vélocité)
 
Lorsque le joueur choisit Ancrage, l’effet « Stabilisé » sera appliqué sur lui à la fin de son tour au lieu de l’être immédiatement. Cela lui permet de se déplacer pendant son tour sans être gêné, puis de profiter d’une immunité aux déplacements hors de son tour.
 
 
Dora N.I.O.
 
L’augmentation du bonus de dégâts progresse de 10% (il passe de 120% à 130%).
 
Malus de Résistances
 
Ils sont désormais limités de manière globale à -200 sur un personnage, toutes sources confondues
 
Restat
 
Désormais, lorsqu’une restat globale est disponible, il n’y a plus besoin d’aller voir un PNJ dans le monde. Il est possible d’accéder directement à la restat depuis l’interface de sorts ainsi que depuis la feuille de personnage.
Une nouvelle possibilité a été implémentée : la restat d’un sort unique. Après des modifications mineures sur une classe suite à un patch (correction de bug, légers rééquilibrages), nous offrirons désormais une restat du ou des sort(s) concerné(s). Lorsque la modification touche davantage de sorts, nous continuerons à offrir une restat globale aux classes concernées.
La limite dans le temps pour procéder au restat a été supprimée.
 
 
Soins en coup critique
 
Ils prennent désormais en compte les dégâts critiques du lanceur
 
Sorts de Résurrection
 
Lors d’une résurrection, l’état « Convalescent » est mis sur le joueur ressuscité.
Convalescent : le joueur ne peut plus être ressuscité. Si son réanimateur meurt, il meurt aussi.
Lors d’une résurrection, le soin n’est plus augmenté via les passifs du lanceur ou de la cible
 
Passif augmentant les PdV
 
Affiche désormais le montant de PdV gagné entre parenthèses
 
Glyphes, bombes, pièges et poisons

Les effets des glyphes, pièges et poisons passent désormais au travers de l’Armure et de la Barrière.

Voici les effets concernés :
 
Glyphe : Trainée de Wakfu
Glyphe : Avalanche
Glyphe : Fécalames
Glyphe : Tapis de Flammes
Glyphe : Microbot (les dégâts de zone)
Glyphe : Mur de Feu
Bombe : Bombe Aveuglante
Bombe : Bombe Immolante
Bombe : Méga-Bombe
Piège : Piège de Brume
Piège : Piège de Lacération
Piège : Piège de Silence
Poison : Intoxiqué
Poison : Maudit
Poison : Tétatoxine
Poison : Crârsenic
Poison : Pupuces Négatives
Poison : Egide Ardente (l’effet de renvoi de dégâts)
Poison : Gangrène
Poison : Cyanose
Poison : Hémorragie
Poison : Mal des Transports
 
 
CARACTÉRISTIQUES

Les personnages gagnent désormais 10 PdV par niveau au lieu de 5 PdV. Ce changement a été opéré pour compenser la perte des résistances dans la caractéristique Force.
 
Intelligence
 
Retrait de la caractéristique Vol de Vie
Retrait de la caractéristique Régénération selon les PdV manquants
Ajout de la caractéristique Barrière
Par point : crée une Barrière (réduction fixe des dégâts subis) de 50% du niveau du joueur pour les n prochains coups (n dépendant du nombre de points investis)
Maximum 10 points investis
Ajout de la caractéristique %Vie en Armure
Par point : en début de combat, ajoute 4% de la vie max en Armure au personnage
Maximum 10 points investis
Soins reçus
N’augmente que les soins reçus d’alliés, pas ceux du lanceur ni de ses propres invocations
Par point : +6% par point (au lieu de 4%)
Maximum 5 points investis (au lieu de 10)
 
Force
 
Retrait des caractéristiques Dégâts à tous les éléments + Résistances mono-élémentaire
Retrait des caractéristiques Points de Vie + Résistances mono-élémentaire
Ajout de la caractéristique Dégâts à tous les éléments
Par point : +5% de dégâts
Ajout de la caractéristique Dégâts monocibles
Par point : +8% de dégâts monocibles
Maximum 20 points investis
Ajout de la caractéristique Dégâts de zone
Par point : +8% de dégâts de zone
Maximum 20 points investis
Ajout de la caractéristique Dégâts de mêlée
Par point : +8% dégâts de mêlée
Maximum 20 points investis
Ajout de la caractéristique Dégâts à distance
Par point : +8% dégâts à distance
Maximum 20 points investis
Ajout de la caractéristique Points de vie
Par point : +20 PdV
 
Chance
 
Dégâts de dos
Par point : +6% dégâts de dos (au lieu de 4%)
Soins
Par point : +6% soins (au lieu de 4%)
Dégâts Berserk
Par point : +8% dégâts Berserk (au lieu de 4%)
Résistance de dos
Maximum 20 points investis
Résistance aux critiques
Maximum 20 points investis
 
Agilité
 
Tacle
Par point : +6 Tacle (au lieu de 3)
Esquive
Par point : +6 Esquive (au lieu de 3)
Tacle et Esquive
Par point : +4 Tacle et +4 Esquive (au lieu de +2 et +2)
Initiative
Par point : +4 Initiative (au lieu de +2)
Maximum 20 points investis
Retrait de la caractéristique Esquive Berserk (< 50% Vie)
Retrait de la caractéristique Tacle multi-cibles
Ajout de la caractéristique Retrait PA et PM
Par point : +2% aux chances de retirer des PA et PM
Maximum 20 points investis
Ajout de la caractéristique Résistance PA et PM
Par point : +2% aux chances de retirer des PA et PM
Maximum 20 points investis
 
 
REFONTE DU SYSTÈME DE SORTS
 
Plusieurs raisons ont conduit à modifier le système de sorts. La principale étant que le système de sorts précédent avait été prévu sur 100 niveaux. Au niveau 175, quelques problèmes sont devenus évidents, notamment l’absence de choix pour le joueur. Par ailleurs, le fait d'avoir des actifs sur 10 niveaux et des passifs sur 20 niveaux imposait beaucoup de contraintes sans réel intérêt.

Nous avons donc pris le parti de faire évoluer le système en réintroduisant la notion de choix, en déclinant les actifs et passifs sur moins de niveaux et en apportant des pics de puissance au joueur lors des déblocages (de sorts, de passifs, de « slots »).
 
 
Le Deck
 
C’est un ensemble de sorts et de passifs que vous choisissez au début d’un combat en fonction de l’adversaire. Il remplace l’ancien système et… il change tout ! En effet, il va vous permettre de mettre en place votre stratégie en gérant vos sorts et vos passifs.
 
Quelques règles tout de même :
 
Il faut avoir placé un sort dans son deck pour qu'il soit utilisable en combat.
Il faut avoir placé un passif dans son deck pour qu'il soit actif en combat.
Vous pouvez à tout moment changer un sort dans votre deck, sauf en combat !
 
Les actifs et les passifs
 
Avant, les actifs et les passifs s’étalaient sur plusieurs niveaux. Nous avons supprimé toute la partie où il était nécessaire de monter ses actifs et ses passifs avec des points à chaque niveau.
Désormais, les actifs ne sont plus que sur un seul niveau. Les passifs, quant à eux, ne sont plus que sur un ou deux niveaux.
 
Chaque actif et chaque passif sont débloqués à un niveau précis du personnage et les passifs évoluent d'eux-mêmes à un niveau donné du personnage. Voici un exemple : un passif débloqué au niveau 20 et donnant +60 d’Esquive évoluerait automatiquement au niveau 120 et donnerait +180 d’Esquive.

L'ensemble de ces déblocages se fait tout au long de l'évolution du personnage, du niveau 1 au niveau 200.
 
 
Les sorts élémentaires
 
Ils ont eux aussi subi quelques modifications substantielles. Nous avons augmenté le nombre de sorts élémentaires jouables en augmentant l'expérience attribuée aux sorts. À titre indicatif, nous sommes partis sur une base de 5 sorts élémentaires au niveau 1, augmentant jusqu'à un total de 10 sorts élémentaires au niveau 200. Dans le même temps, nous avons supprimé la contrainte qui faisait en sorte qu'un personnage mono-élément ait moins de sorts jouables qu'un personnage multi-éléments, qui n'avait plus lieu d'être dans ce nouveau système.
 
Nous avons également décidé de replacer le système de « lock de sort » (le cadenas) au niveau 100. Le but de cette modification est d'en faire une des récompenses du 100+, qui permet de moduler facilement son build au gré des envies.
 
En effet, avec les modifications réalisées au précédent patch sur ce point, il est désormais plus rapide de diminuer le niveau d'un sort au profit d'autres sorts dans le but de changer d'orientation.
 
  
REFONTE DE KATREPAT
 
Modification géographique totale de l'île :
 
Ajout d'une nouvelle famille écosystème : la famille des Croques-Miteux (niveau 70+)
Ajout d'un Donjon pour la famille des Croques-Miteux : le Misolée (cf. section « Donjons »)
Ajout de Quêtes des Chasseurs des Disciples d'Otomaï pour la famille des Croques-Miteux
Ajout d'un Comptoir de Mercenaires
Ajout d'Exploits
 
 
DONJONS
 
Ajout d'un donjon La Patapoutrerie à Brâkmar, dans les Landes de Sidimote, pour la famille de monstres Patapoutres.
La clé de la Patapoutrerie pourra être obtenue sur les monstres de la famille Patapoutres de Brâkmar, dans les Landes de Sidimote
Le donjon a un prérequis d'entrée de niveau 52
Le donjon propose 4 salles
4 nouveaux membres de la famille Patapoutre et un nouveau boss sont à découvrir en exclusivité dans ce donjon
Il comporte 5 exploits et un meta-exploit associé
Une douzaine de nouveaux équipements (de rares à mythiques)
Ajout d'un donjon Le Misolée à Katrepat, pour la nouvelle famille des Croques-Miteux.
La clé du Misolée pourra être obtenue sur les monstres de la famille Croques-Miteux de Katrepat
Le donjon a un prérequis d'entrée de niveau 77
Le donjon propose 4 salles
 
NATIONS
 
Ajout du premier chapitre sur cinq de la Quête Principale des Nations :
Cette quête a pour but de guider les joueurs au travers des Nations, dans des zones en rapport avec leur niveau
Elle permet aussi de présenter la philosophie et les modes de vie de chacune des nations et de marquer leur mentalité
Elle offrira au terme du cinquième chapitre d'acquérir un nouvel artefact divin
L'artefact permettra d'explorer le premier palier du Mont Zinit plus avant que la partie présentée dans le Chapitre 1 des Nations
Chaque chapitre ouvre l'accès à un Donjon Aventure exclusif de type "easy" (conseillé pour 3 personnages)
Chaque Donjon Aventure permet d'acquérir de l'équipement "aventure" spécifique
Modifications mineures des zones de départ des Nations (Ruines, Zone bouftou, Zone Tofu) en relation avec la Quête Principale des Nations 
 
MONT ZINIT
 
Désactivation des anciennes zones du Mont Zinit (Plage, Gloutblop, mini-donjon Aguabrial, Grottes Patapoutres, Gris XIII vs. Rabet & Co., Paroi du vaisseau, Penitenslek, galerie Slek)
Ajout de nouvelles zones (liées au Chapitre 1 de la Quête Principale des Nations)
 
ASTRUB
 
Modification mineure de la fin de la Quête Principale d'Astrub afin d'assurer une bonne transition avec la Quête Principale des Nations.
Modifications mineures des zones des Plaines d'Astrubs en relation avec la Quête Principale des Nations.
 
 
HAVRE-MONDE
 
Sols Neige de Nowel : en poser une grande quantité ne provoquera plus de déconnexion.
 
JCJ
 
Ajout d'arènes JcJ : Dimension Ecaflipus
 
Qu’est-ce que c’est ?

De nouvelles maps spécialement dédiées pour les combats Joueur(s) contre Joueur(s) ont été mis en place dans Ecaflipus !

Elles faciliteront les défis amicaux, ainsi que l’organisation de tournois ! Attention toutefois, il ne s’agit pas de « JcJ classé ».

 
Comment s’y rendre ?

Vous pouvez accéder à Ecaflipus en cliquant sur la grande Statue d’Ecaflip qui est apparue à Astrub, à la place de l’ancien « ring d’Astrub » !

Elle est indiquée sur votre carte, sous le nom de « Ecaflipus (Arènes) ».

 
Comment cela marche-t-il exactement ?

Les maps sont d’ambiance et de tailles radicalement différentes et elles sont équilibrées pour le JcJ. Elles sont symétriques, il y en a des petites, des grandes, avec beaucoup ou peu d’obstacles, des placements rapprochés ou éloignés, regroupés ou dispersés, etc.

Le Level Design des maps est prévu pour être clair sans avoir besoin de passer par le mode tactique : toutes les cellules sont bien visibles et cliquables, les zones bloquant le mouvement et la ligne de vue sont représentées par des obstacles tandis que les zones bloquant seulement le mouvement sont représentées par des trous dans le sol.

Lorsque vous lancerez un défi, cela créera une bulle de combat avec un nombre de positions de départ dépendant du nombre de joueurs dans votre équipe. Vous devez donc créer votre groupe de combat avant de lancer le défi.
 
Note importante : En conséquence, les duels ne seront plus possibles devant le Bibliotemple, l’objectif étant d’alléger le serveur d’Astrub, et de déplacer les duels dans l’espace qui lui est dédié sur Ecaflipus.
 
 
Xélor VS Xélor

Le problème avec la Distorsion a été corrigé, et le Xélor touché pourra continuer à jouer normalement.

 
Obtention des objets PvP dans les croupiers

Un critère a été ajouté pour vérifier le niveau du personnage pour obtenir un objet dans le croupier. Par exemple, pour pouvoir prendre un objet de niveau 120 dans le croupier, il faut maintenant être de niveau 120.
 
 
EXPLOITS / QUÊTES
 
Challenge Sédentaire : échouait à cause des malus PM. Cela a été corrigé grâce au remplacement des pertes de PM par un Deboost.
Héros des Mérydes (titre obtenu lors de l'event Pwak) : a été renommé en « Le Roi / La Reine des Cloches ».
Contrat de mercenaire : La quête Sufokia - Les Jardin Suspendus - Contrat Rang - "C'est la scaratastrophe !" ne pouvait pas être prise, cela a été corrigé.
 
MONSTRES
 
Etat "Cyclone" : L'état des Tornados Chefs, Tornades, Diskoors et Tempêtes peut maintenant se cumuler 5 fois.
Mihmol : Bébétard ("etat" cartes tiré par l'Ecaflip en combat) ne soignera plus en Mihmol.
​Goules : La famille des "goules" affichera maintenant la bonne icône dans l'interface écosystème de la région Katrepat.
Remplacement des Pertes PA/PM par un Deboost : le fonctionnement des pertes PA et PM a été uniformisé. Cela a permis de corriger de nombreux de bugs, notamment sur les challenges et sur l’hypermouvement / hyperaction.
 Les sorts concernés sont les suivants:
Picorage Céleste
Picorage Sauvage
Javelot
Kamitaliste
Gaz Putwide
Larcin Motivé (Boss Encap)
Déchainement Pioussant
Dégoulage
Châtiment fongique
Martèlement Bicéphaloïde
Explocri
Shulbute
Cactification
 Les zones d'effets concernées sont les suivantes :
Laine de Bouftou
Caramel
Boule de Caramel
Terreau Céleste
Flaque de Sève Riche
Flaque d'eau
Petite Hélice - Centre
Baveur Fou
Glaglacération
Bilbyalgues Perlouzées
Invasion de Gelées
Sporules Explosives
Explosion Fongique
Pas de Fwoussawds!
Hägen-Glass : étant donné le niveau du boss, et le fait que les dégâts de collision spammables sur les classes ont été retirés, le nombre de stack de l'armure a été diminué pour éviter un combat inutilement long.
Détails :
Premier min HP : 15-20 stacks (au lieu de 35-50)
Second min HP : 9-14 stacks (au lieu de 20-34)
Troisième min HP : 1-8 stacks (au lieu de 1-19)
1ere Perte de PdV dans l'armure : proc à 14 stacks (au lieu de 34)
2eme perte de PdV dans l'armure : proc à 8 stacks (au lieu de 19)
 ​Boost de 1 PM : 13-16 stacks (au lieu de 31-40)
Boost de 2 PM : 9-12 stacks (au lieu de 21-30)
Boost de 3 PM : 5-8 stacks (au lieu de 11-20)
Boost de 4 PM : 1-4 stacks (au lieu de 1-10)
 
OBJETS
 
Bibliotéléport : il permettait auparavant de sortir de prison (sans utiliser la lime), cela a été corrigé (et le critère "ne pas être en prison" a été rajouté dans les conditions).
Miniature de Serre d'Acier : l'ombre de la miniature de Serre d'Acier a été corrigée et apparaîtra correctement à présent.
Dora Lagoole : elle est à nouveau comptée dans la limitation à un épique.
Croupier Wabbit : les objets qui apparaissaient en double ont été supprimés.
Feu de l'Amour (objet) : il peut désormais être supprimé
Frizzette (objet / conso) : elle est désormais stackable par 999
Crampomy (objet / conso) : il est désormais stackable par 999
Visserie (objet / craft) : il peut désormais être crafté par lot de 5
Cape en série limitée : désormais disponible dans le croupier de la Corbeau Cave
Brisage des objets JcJ : ces objets donneront désormais la même quantité de runes que les objets mythiques
 
 
FAMILIERS
 
Corbac : les nourritures consommées par le Corbac seront bien visibles sur sa fiche descriptive.
Krosmunster : les joueurs pourront dorénavant donner du  « Pain de Mie » (craftable en jeu) aux familliers Krosmunster
 
STEAM
 
Succès : le Succès "Fusionner 100 fragments" ne se valide pas avec les fragments liés. La précision a été ajoutée sur tous les exploits de Reliques.
 
HÔTEL DES VENTES
 
Limite de Kamas : lorsque la limite de kamas pouvant être possédés ou récupérés a été atteinte, le joueur recevra une notification (pour éviter toute perte).
 
INTERFACE
 
Titre : le problème d'affichage du titre du succès de métier "Maître d'Armes à Distance" a été corrigé : les titres trop longs ne seront plus barrés.
Espace Cadeaux : par le passé, posséder trop de cadeaux empêchait l'apparition de l'Espace cadeaux en jeu. La limite a été augmentée et l'espace ne disparaîtra plus. En revanche, une fois la limite atteinte, la suite des cadeaux n'apparaîtra pas immédiatement. Un message d'erreur dans le chat indiquera au joueur qu'il doit récupérer ses cadeaux pour avoir la suite.
Métier Trappeur : rajout des infos manquantes sur les semences de Gloutoblop.
Notifications : Les notifications du bas de l’écran ayant un titre trop long n’auront désormais plus un affichage déformé ou empiétant sur la ligne de démarcation.
 
DIVERS
 
Java : La version de java embarquée avec le client est mise à jour en version 8 suite à la fin des mises à jour de la version 7 par Oracle.
Souci avec les configurations lors de changement de compte : quand on connectait un premier compte puis qu'on revenait sur la page de login pour se connecter avec un second compte, ce sont les informations de connexion du premier compte qui étaient conservées. Cela a été corrigé.