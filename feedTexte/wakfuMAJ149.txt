CONTENU PRINCIPAL
 
Niveau Modulable & Builds Rapides
 
Le niveau modulable (aussi appelé downscaling) est la capacité d'un joueur à réduire virtuellement le niveau de son personnage pour réaliser des activités, tout en obtenant des récompenses qui s'adaptent au vrai niveau du personnage.
 
Cette possibilité sera incorporée dans le système de builds rapides. Comme leur nom l'indique, les builds rapides permettent de changer de build en un seul clic, sans ouvrir la moindre interface.
 
Mentorat
 
Il s'agit de la phase post-parrainage : l'accompagnement des nouveaux joueurs en début de jeu.
Le parrain accompagne son filleul en jeu pour le faire progresser, mais en se mettant à son niveau pour ne pas lui ruiner son expérience de jeu.
 
De manière générale, nous récompenserons également tous joueurs passant du temps à jouer avec des débutants.
 
ÉVÉNEMENTS
 
Halouine
 
Les empilements maximum d'objets de l'évènement Halouine sont passés de 99 à 9999.
Le nombre de monstres d'Halouine qui apparaîtront est considérablement augmenté :
Toutes les 3 minutes (au lieu de 5), le système vérifie qu'il n'y a plus assez de monstres dans la zone pour en réinvoquer.
S'il a moins de 60 monstres (au lieu de 40), le système en réinvoque.
Le système réinvoque 40 monstres à la fois (au lieu de 18).
Ces valeurs sont par zone. Les zones concernées sont les zones d'Astrub suivantes : Montagne, Champs, Plaine, Cimetière, Forêt et Tainéla. 
 
MONDE
 
Astrub
 
Les ambiances de nuit ont été entièrement retravaillées, avec l'ajout de nombreuses sources de lumière rendant la ville plus vivante.
 
QUÊTES
 
Quête de l’histoire Dunîl
 
Les joueurs qui avaient obtenu l'Entrée 1 du journal de Bord de l'Arène Dunîl sans avoir le niveau requis pour commencer la quête associée pourront correctement commencer la quête en obtenant à nouveau l'objet.
 
Mercenaires
 
Les Quêtes Mercenaires des 4 Nations concernant le Tofulailler peuvent à nouveau être validées.
Un marqueur carte a été ajouté sur la quête de l’île de Moon – Surface Moon – Profondeur.
 
Collaboratif
 
L’apparition des monstres en zones inaccessibles dans la quête "collaboratif : Invasion de Pilleurs du Culte d'Ogrest" a été corrigée.
 
Divers
 
Les PNJ Contremaître Piokito et Tati Hacheum donneront les bonnes ressources à utiliser pour confectionner les composants lors de la quête Dragodinde.
 
OBJETS
 
Les « Anciens Jetons des Tropikes » qui n'auraient pas été convertis en « Jetons de l'île de moon » seront supprimés. Nous vous invitons à les échanger dans n'importe quel croupier de l'île de Moon avant la mise à jour.
Il est désormais possible d'échanger la Relique « Epaulettes du Roi Singe » contre une autre version de celle-ci dans le croupier du Boss Ultime de l'île de Moon.
La recette de l'Arôme Sanguin ne nécessite plus de Sang de Tofu. Le Coutelas Racroft, qui nécessitait des Arômes Sanguins, nécessite désormais 3 Sangs de Tofu en plus.
La Bave de Bouftou — qui n'est plus obtenable et a été supprimée — est remplacée par les mêmes quantités de Laine de Bouftou dans les recettes. L'action de récolte sur les Bouftous Nuageux indiquera bien une récolte de Laine de Bouftou et non pas une récolte de Bave de Bouftou.
Correction du Feu de l'Amour et autres objets de lumière : ils sont désormais utilisables de nuit OU dans les mines, souterrains et égouts.
La cape des Ailes d'Orrok s'affiche de nouveau correctement.
Les incohérences concernant les récoltes de semences de monstres (niveaux affichés) sont corrigées.
Les composants intermédiaires ont maintenant tous une description.
La recette de l'Atelier de Tailleur demande 3 Chapeaux du Chaos au lieu de 3 Chapeau du Pandawan. Les recettes des 3 objets de la panoplie Pandawan ont été retirées pour le moment. Il n'est pas impossible que la panoplie soit à nouveau disponible un jour.
Le poison « Maudit » posé par le sort « Relent Boisé » sera désappliqué lorsque la cible passe K.O.
La recette de la Table Iop ne nécessite plus d'épée qui ne peut plus être confectionnée.
La recette du Petit bâton de Fryou voit son nombre de Bâton Tofu nécessaire à la recette réduit de 33 à 6.
Le Costume de Wakfu aura désormais un visuel dans l'inventaire. Il devient lié au compte.
Le Plan « Epaulettes du Roi Singe » devient "lié au compte" plutôt que "non-échangeable". De plus, il peut être jeté ou supprimé.
 
MONSTRES
 
L'archimonstre Lartifice l'Explosive apparaît correctement dans son groupe lorsque les conditions sont valides.
 
DONJONS
 
Le Donjon de la Trool Académie est de nouveau accessible.
Donjon Opaket : Le trou beau du berger - Mimi la Merveilleuse a maintenant cessé de taper sur ses alliés.
 
MÉTIERS
 
Les menus au clic-droit ne seront plus inversés sur les étapes possédant un fruit pour le Noisetier et le Châtaignier.
 
DIVERS
 
Les Donjons Tofu et Bouftou à poser en Havre-Monde fonctionneront à nouveau.
Le Bateau de Matou Meuplet ramène de nouveau le joueur à Astrub au retour du Mont Zinit.
Les problèmes d'animation de la monture Mulou ont été corrigés. Une animation de saut a été ajoutée.
Introduction du Finisher via le « Finisher Otomaï » gratuit à obtenir auprès d'Otomaï à Incarnam !