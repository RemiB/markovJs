CONTENU MAJEUR

 

Ouginaks :

Vous les attendiez, nos amis à poils arrivent en jeu pour votre plus grand plaisir ! Venez découvrir cette nouvelle classe au caractère agressif !

L’ouginak apporte avec lui deux nouvelles quêtes liées à la classe : une narrative et une répétable.

La quête narrative se déroule en partie dans une nouvelle zone, le Campement Ouginak, situé sur Saharach. Elle démarre au niveau 56 et permet d'obtenir un nouveau sort.
La classe sera seulement accessible aux joueurs sous booster. En effet, si vous possédez un booster actif, vous pourrez la sélectionner lors de la création de personnage. Une fois votre booster terminé, vous conserverez l’accès à votre personnage. Attention : si vous aviez accès à l’Ouginak dans la bêta mais que vous ne possédez pas de booster actif sur les serveurs de jeu, vous n’aurez pas accès à la classe.
 

Rééquilibrage de Classes


Refonte Enutrof

Nos amis Enutrofs se sont offerts une petite cure de vieillissement. Pour eux, il n'est pas question d'aller dans une maison de retraite de sitôt ! Au contraire, ces chercheurs de trésors ont retrouvé des mécaniques stables et innovantes. De quoi continuer leur vie d'aventurier encore bien longtemps…


Augmentation de la valeur des sorts de soins 

Afin de rendre plus significatifs les soins à bas niveau, nous avons multiplié leur base par 4.La modification concerne également les invocations génériques (Gonflable, Gobgob, Lapino...), les passifs, et les états.

Exceptions :

Les effets de Vol de Vie.
Les effets de Soin Fixe (type "soigne X% des points de vie actuels de la cible").
Jacasserie, Pince du Crabtor et Esprit du Crocodaille de l'Osamodas : leur base a été multipliée par 2 et leur incrément a été divisé par 2 afin de coller aux valeurs de soin normales.
A la Masse du Steamer ne change pas, puisqu'il s'agit techniquement d'un gain d'armure.
Maîtrise Zobal : La valeur du soin par dommages de collision au niveau 2 du passif a été multipliée par 1,5. Cette nouvelle valeur est aussi celle du passif à son niveau 1.
Arnaque Sanglante du Sram : Soins multipliés par 2 à tous les niveaux.
Ouvrir les Veines du Sram : Soins multipliés par 2,77 à tous les niveaux.
Capucine de l'Ecaflip : la valeur de Pupuces positives donnée lorsque l'Ecaflip se cible lui-même est multipliée par 2.
Compagnons : Les sorts de soin ont été rééquilibrés afin de posséder la même base de soin qu'une classe (soit un incrément faible mais une base forte). En outre, les Aptitudes des Compagnons possédant des capacités de soin comportent désormais un bonus de 30% Soins Réalisés.

Modifications sur le fonctionnement du retrait PA/PM 

Nous avons totalement changé le système de retrait de PA et de PM.

Désormais, les chances de retirer des PA et PM seront moins aléatoires et elles seront dépendantes d’une caractéristique unique : la Volonté.

La Volonté remplace les caractéristiques suivantes : % Résistance PA, % Résistance PM, % Retrait PA, % Retrait PM. 1 Volonté a le même effet que 1% dans toutes ces anciennes caractéristiques.Sans plus attendre, voici la formule utilisée : le retrait de x PA (ou PM !) est déterminé comme suit : x * y * 0,5 où yest une valeur comprise entre 0 et 2 qui compare la Volonté du lanceur à la Volonté de sa cible, selon la formule y = ( 1 + Volonté du lanceur / 100 - Volonté de la cible / 100 ).

En somme, votre cible résiste de base à 50% des pertes de PA/PM que vous tentez de lui infliger. Ne vous inquiétez pas toutefois, de nombreux rééquilibrages sur les valeurs de PA/PM retirées sont effectués, ainsi que des modifications sur les gains et pertes de Volonté.

Pour éviter que votre cible puisse se faire « légumiser » trop facilement, elle gagne de la Volonté à chaque fois qu’elle perd des PA ou PM, mais en valeur moins élevée que l’Hyperaction ou Hypermouvement actuel. Par contre, il sera compliqué de faire perdre des PA et des PM à une même cible sur un même tour.

Cette Volonté va également augmenter son potentiel de réponse pour faire perdre des PA ou PM à son tour, malgré le fait qu’elle possède moins de PA ou PM pour effectuer ses actions.

Pour terminer, une nouvelle génération de retrait PA voit le jour : les effets de poussée n’infligeront plus de dommages lors des collisions mais retireront 1 PA aux cibles de la collision à la place indépendamment du nombre de cases parcourues, du moment que la cible heurte un obstacle. Si une cible est poussée contre une autre, la collision fera alors perdre 1 PA à ces deux cibles. Des modifications sur les sorts de poussée sont effectuées en fonction.
A noter que tous les effets de poussée du jeu occasionnent désormais des collisions (sauf gameplay spécifique : sur les quêtes par exemple). Les effets d'attirance, de rapprochement ou encore d'éloignement ne provoquent en revanche pas de collision.

Dans les Aptitudes de personnages (Agilité), les %Retrait et %Résistance PA/PM sont retirés. Une nouvelle caractéristique voit le jour (Volonté) et donne 1 de Volonté par point dépensé dans l'aptitude. Les points investis dans les anciennes caractéristiques seront rendus.
 

La Famille des Riktus Élite


Nous avons charbonné pour réhabiliter les Riktus Elite et leur antre, et ils ont bonne mine. Un Riktus Elite, selon comment c'est tourné, ça change tout ! Ne vous avisez pas de croiser leur regard... ou leur côté... ou leur dos. Leur boss est aussi tout feu tout flamme, et son tempérament est on ne peut plus explosif !

 

CLASSES


Correction sur divers scripts de sorts de classes, afin d'afficher la totalité de l'animation du sort et de corriger les cas où un double transparent apparaissait sur la cible du sort.

Téléportono du Pandawa
Bond du Iop
Fêlure du Zobal
Pied Rocheux et Tatoo Ondoyant du Sacrieur
Pilonnage du Steamer
 

Feca

Provocation

Le sort n'applique plus "Provoqué". Le sort retire -2PM (non-esquivable) au lieu de tous les PM.
Cuiracier  

Le bonus de % Retrait de PM devient +20 Volonté.
Elémentor

N’augmente plus la durée des glyphes sur le terrain. Augmente de 1 les retraits d’Avalanche et de Fécalames.

Xelor

Horloge

Retire 1 PA et non 2.
Sablier

Le sort ne coûte plus qu'1 PA, et ses dégâts sont revus à la baisse en conséquence. Le Xélor se retire 2 PA, puis tente un retrait de 2 PA sur sa cible. Les dégâts et l’application de gel temporel sont modifiés en conséquence. Le sort regagne son effet précédent en tour Tique (don de PA). Les autres effets du sort ne changent pas.
Maître Horloger

Le bonus de % Retrait de PA devient +20 Volonté.
Vol du Temps

Si un retrait de plusieurs PA d'un coup est effectué, l'état monte d'autant de niveaux.


Iop

Cogneur

Le bonus de % Résistance de PM devient un bonus de Volonté (+1 par niveau de l'état).

Cra

 

Balises

Balise Crâ : Posséder ce sort dans son deck débloque le sort Débalisage dans la troisième barre de sorts.
Débalisage : Retire la Balise ciblée. Le sort coûte 2 PA, et est limité à 1 utilisation par tour.
Le nombre de Balises sur le terrain est uniquement influencé par la caractéristique Contrôle du Crâ, et plus par un nombre max prédéfini.

Roulade

Ne donne plus 2 PM au Crâ.

Oeil de Lynx

Passe à 4 tours de recharge. Le bonus de Portée pour le Crâ et ses alliés est de +2, et dure désormais 2 tours.

Retraite Balisée

Permet au Crâ de se téléporter sur une case adjacente à une Balise. N'échange plus de position avec celle-ci.

Flèche Explosive

Limitation à 1 lancer par tour.

Pluie de Flèches

Sa portée passe à 4-6 non-modifiable. Son effet lorsque le Crâ est sous l'emprise de Mordant est modifié : le sort retire désormais 1 à 3 PM en fonction du niveau de Mordant, et consomme Mordant.

Flèche Chercheuse

Ne retire plus d'Armure.

Flèche Harcelante

N'est plus sans ligne de vue. Le sort retire de l'Armure à sa cible.
Balise Air

Ne peut plus pousser une même cible que deux fois par tour.
Flèche Immolante

Ne pousse plus.
Frappe Incisive

Donne +40 Volonté pour le tour en cours du Cra
Mordant

Retrait du bonus de % Retrait de PM
Optimisation des descriptions. Les effets des sorts sont plus clairs et prennent moins de place, afin d'être plus agréables à la lecture.

Sram

Augmentation des valeurs de soin du passif Assassin, ainsi que des sorts Ouvrir les Veines et Arnaque Sanglante, en accord avec le rééquilibrage des soins.
Châtiment

Applique désormais l'état Châtiment (augmente de 10 à 20% les dommages indirects reçus par la cible pendant 1 tour).
Mise à Mort

Augmentation du bonus de dommages si la cible est affaiblie (passage de 10 à 20).
Kleptosram

Ne génère plus de butin. Vole 1 PM.
Filouterie

Ne génère plus de butin. Vole de l'Armure sur les cibles de la zone.
Attaque Perfide

Ne retire plus d'armure. Vole 1 PA.
Escrosramerie

Ne génère plus de butin. Vole de l’esquive (valeur fixe, 50% du niveau du Sram).
Invisibilité

+2 PM sur le Sram à l'utilisation du sort (tour en cours)
Sort Actif Larcin

Disparait pour devenir "Aller".
Aller

Le sort Aller peut être utilisé sur le Double afin d'échanger de position. Une fois Aller lancé, le Sram ne peut plus utiliser aucun sort durant son tour, hormis Peur et Retour. (Posséder ce sort dans son deck débloque le sort Retour dans la troisième barre de sorts).
Retour

Peut être utilisé après Aller sur le Double afin d'échanger de position. Le Sram peut ensuite à nouveau utiliser tous ses sorts.
Assaut Brutal

Au niveau 1 : 10% Dommages infligés sur Arnaque Sanglante, Traumatisme et Mise à Mort lorsque le Sram possède 100 de Point Faible. Le bonus devient 20% au niveau 2 du passif.
Maître des Ombres

Lorsque le Sram se cible lui-même avec Invisible, il gagne 10% de ses PdV max en Armure pour 1 tour (15% au niveau 2 du passif). Augmentation des valeurs de dommages à la sortie d'Invisibilité : passe de 25 à 50% au niveau 1 du passif, et de 50 à 75% au niveau 2 du passif.
Attaque Perfide

Limitation à 2 par cible.
Un sram invisible sera affiché à la bonne position quand son invisibilité se termine
 

Duperie

Le bonus sur Larcin devient +20 Volonté

Sadida

Herbes Folles

La perte de PM devient -3 PM (niveau 0) à -6 PM (niveau 120 et plus). La zone devient une ligne de taille 3. Le nombre de tours de recharge augmente de 1.
Venimeux

Le bonus de % Retrait de PM devient +20 Volonté.
Eveil Sylvestre

Appliquera 30% des PdV max du Sadida en Armure à la cible (1 tour) en plus de ses autres effets.
 


Zobal

Psycose

La perte de PM devient -2 PM lorsque le Zobal porte le Masque du Psychopathe.
Masque du Psychopathe

Le gain de % Dommages infligés passe de 15 à 20. L'effet qui rendait de la vie aux alliés proches lorsque le Zobal subissait des dommages devient un bonus de 30% vol de vie sur les sorts Psycate, Dard Dare et Clasmada.
Maîtrise Zobal :

Le gain de % Dommages de collision devient "Après avoir créé une collision entre deux cibles, le Zobal gagne 20% Dommages infligés pour le tour en cours" (25% au niveau 2 du passif).
Elégance

Le bonus au niveau 20 devient "Dantine : Pousse de 3 cases".
Dantine

Effectue des dégâts air monocible en plus de ses autres effets.
Le bonus au niveau 40 devient "Clasmada

Réduit de 1 le coût en PA".
Le Zobal est immunisé aux collisions de son double, et inversement.


Eliotrope

Wakméha

La perte de PA devient -2 PA.

Huppermage

Vestige

La perte de PA devient -3 PA.
Feu-Follet

Le bonus de %Résistance de PA et PM devient +40 Volonté.
Runes de l'Huppermage

Elles augmenteront ses maîtrises en prenant correctement en compte les bonus de Maîtrise élémentaire (générale à tous les éléments).

Sacrieur

Le sort Assaut passera sans ligne de vue dès le niveau 1 du passif Mobilité (et non plus au niveau 2), comme il est indiqué dans la description dudit passif.

Ecaflip

Le chacha de l'Ecaflip attaquera bien les monstres 9 cases.

Eniripsa

La Marque Eting

Régénère correctement la BQ de l'Huppermage si celui-ci est proche de la cible lorsqu'elle meurt.
Transgression

Augmente les dommages du prochain sort, et non plus les dommages de tous les sorts du tour.
Lapino

Le Mot de Frayeur du Lapino passe à 2 lancers par cible.

Roublard

Tir Surprise

Ne se déclenche plus si le Roublard est sous forme d'arbre.
Perforateur

Le bonus de %Retrait de PM devient +1 Volonté par niveau de l'état.

Steamer

Le gain de PW de Stasifié se déclenche maintenant quand la cible passe KO et non plus à sa mort.

Osamodas

Les invocations de l'Osamodas ressuscitées par Esprit Phénix ne joueront plus leur tour immédiatement après leur résurrection, mais attendront correctement leur tour d'origine dans la timeline.
Passif

Motivation

Le bonus de %Retrait de PA et PM devient +10 Volonté.
 

OBJETS


Panoplie du Pandawan

La Panoplie du Pandawan devient une panoplie de la tranche 111-125. Elle peut être récupérée sur les archimonstres et dominants de la Shukrute.

Bottines Écailleuses

Les Bottines Écailleuses version rare ne pourront plus être obtenues sur des monstres Crocodailles classiques. Aucune recette amélioration pour cet objet n'est ajoutée puisque la version mythique était et restera obtenable sur le monstre dominant des Crocodailles.

Cape Lodir

La Cape Lodir de rareté Légendaire pourra correctement être améliorée en objet Souvenir. Les caractéristiques de la Cape Lodir (Mythique & Légendaire) sont également revues pour apporter des bonus proportionnels à leur niveau et leur rareté.

Objets Souvenir

Ajout de 3 nouveaux objets Souvenir : Plastron Vampyrique, Epaulettes Vampyriques et Harmonie.
Chacha Poisseux

Le bonus de % Retrait de PA/PM devient un bonus de Volonté.
Chacha Ventru

Le bonus de Points de Vie devient un bonus de Maîtrise Elémentaire.
Le bonus de % Retrait de PA/PM devient un bonus de Volonté.
Runes Fabuleuses

Les Runes Fabuleuses d'Atrophie et de Détermination donnent désormais de la Volonté, jusqu’à 5 Volonté par rune au niveau 10.
Les Métamorfos d’Atrophie n’existent plus et sont à transformer en Métamorfos de Détermination.
Les recettes qui donnaient des Métamorfos d’Atrophie créent désormais des Métamorfos de Détermination.
Monture Mulou

La monture Mulou peut être obtenue au rang 5 de compagnie et non plus au rang 8.
Rune Relique d’Alternance

La rune augmentera les maîtrises de son porteur en prenant correctement en compte les bonus de Maîtrise élémentaire, dite "Maîtrise ALL".
 

QUÊTES


Camps de Chasseurs

Des Camps de la guilde des chasseurs ont été ajoutés sur Srambad et Enutrosor.

Ecole d’Huppermagie

Lors de la quête "Point de Lumière sans Ombre", le combat contre les Encapuchonnés se déclenchera bien lorsqu'un personnage à la bonne étape de la quête interagit avec le bon casier, même si un des personnages n'est pas à la bonne étape de quête.

Commerce d'Essence Ethernelle

La quête répétable de la Guilde des Chasseurs donnera bien 2 jetons à sa complétion au lieu de 4.
Mercenaires

Correction sur les boussoles du Contrat rang 1 - Vandales des plaines des Mercenaires de Bonta (Plaines de Cania).
Interagir avec les Prespics du Bois de Gueule à Brâkmar validera bien les objectifs du contrat pour les Héros.
 

MONSTRES


Viticultistes

Dans le Vignoble Ignoble, les Serprieuses sont désormais correctement orientées après activation de "Garde du Corps" (et non plus uniquement visuellement).

Phorreurs de Zinit

Les sorts de charge des Phorreurs ne pourront plus être effectués lorsque ceux-ci sont stabilisés.

Blérox de Zinit

Les armes ne permettront plus d'outrepasser les différentes mécaniques de renvoi de dommages de la famille.
Fouirox : Sournoiserie ne pourra plus être lancé sur une cellule vide.
Sor’Hon, Seigneur de la Flamme

Les armes ne permettront plus d'outrepasser le renvoi de dommages du boss.
Gloutos du Zinit

L'état Glouglouglou fonctionnera comme prévu et ne réduira plus de 100% les dommages infligés en mêlée.
Les monstres perdront en résistance s'ils sont confrontés à un seul joueur.
Mulous et Mulous-Garou

La famille des mulous et des mulous-garous a été passée sur le nouveau modèle de monstres. Les mulous sont maintenant du même niveau que les mulous-garou.
 
OGREST

Marteau de Darkli Moon

Les effets du Marteau de Darkli Moon seront bien désappliqués au passage en Phase Turquoise.

Choc Vital

Choc Vital n'infligera plus de dégâts anormalement élevés lorsque son porteur possède des gains de % Dommages indirects.
Larmes d’Ogrest :

Le challenge Symbiote ne donnera plus de larmes aux joueurs lorsqu'il est échoué.
Les Challenges n'apparaîtront que si un joueur de niveau 36 ou plus est dans le combat.
 

DIVERS


Havre-Monde :

Les éléments interactifs qui permettaient de consulter les Enchères de Havre-Monde ont été retirés des nations.
Les "Anciennes semences de Boo" posées dans des Bitonio de Havre-monde seront supprimées automatiquement.
Il en sera de même pour tout prochain objet de types semence qui sera modifié et n'aura plus pour action de planter un groupe de monstres.
Les groupes de monstres plantés en Havre-Monde apparaîtront maintenant de manière aléatoire et non plus selon un schéma prédictible

Bibliotemple :

Ajout des Chaos d'Ogrest sur le Bibliotemple.
Interface de Récompenses

Modification de l’interface de récompenses : (Ces modifications sont faites dans le but de généraliser les récompenses pour de futures utilisations)

Elle est déplacée vers le menu de gestion de quêtes.
Accessible via le raccourci Shift+R.
L’origine de la récompense est indiquée dans la description.
Ajout d’un champ filtre.

Textes :

Correction de quelques fautes de frappe ou d'orthographe reportées par les joueurs.

Machines :

Les 2 Machines à Boonay de la taverne d'Astrub ont été déplacées au sous-sol de la taverne.
Correction du problème de timeline en début de combat.
Le bouton de la recherche de groupe est maintenant fonctionnel.
Recherche de groupe

Le bouton d'accès à la recherche de groupe est maintenant fonctionnel
 

 

OPTIMISATION :

Suite aux données collectées grâce au dernier Happy Hour, nous avons procédé à une optimisation de la couche réseau des effets (dégâts, soins, costumes, etc). Nous notons une réduction d'environ 20% du poids réseau.

Les effets étant un des aspects les plus gourmand en réseau du jeu et ayant un impact considérable sur les performances du client sous conditions, le jeu devrait être plus stable sur certaines configurations
Les zones pouvant accueillir de nombreux combats en simultané (archimonstres de Moon par exemple) pourraient être notablement plus stables
La reconnexion en combat est également concernée, tous les effets étant envoyés au client lors de l'arrivée en jeu. La procédure devrait désormais être moins lourde pour le client.
La stabilité du jeu lorsque de nombreux joueurs arrivent dans les alentours de votre personnage pourrait également être améliorée sur certaines configurations

Il est relativement compliqué pour nous d'être plus précis : les effets touchent une très large partie du jeu et la configuration de vos ordinateurs peut fortement impacter les effets de ces modifications. De façon générale, il ne devrait pas y avoir de cas de régression des performances du jeu suite à ces modifications.Nous avons également réalisé quelques améliorations supplémentaires du côté des serveurs afin d'alléger la charge durant les événements multipliant l'expérience gagnée, implicitement, votre expérience de jeu (pas de personnage) devrait être meilleure.Améliorer votre confort de jeu est une de nos priorités, même si des améliorations sont réalisées à chaque patch, nous souhaitons renforcer nos efforts sur les performances du client et des serveurs, et vous fournir des informations plus transparentes sur leurs apports au fil des patchs.