function mapTextToWordsAndPunctuation(text)
{
    const wordsAndPunct = [];
    const punctuation = [".", ","];
    const lineJump = ["\n"];
    const spaces = [" "];
    const toIgnore = ["\r", "(", ")", "\""];
    let currentIndex = 0;
    text.split("").forEach((char, index) =>
    {
        if(toIgnore.includes(char))
        {
        } else
        if(lineJump.includes(char))
        {
            currentIndex+=(wordsAndPunct[currentIndex])?1:0;
            wordsAndPunct[currentIndex] ? wordsAndPunct[currentIndex] += char : wordsAndPunct[currentIndex] = char;
            currentIndex++;
        } else
        if(punctuation.includes(char))
        {   
            if(text[index+1] && spaces.includes(text[index+1]))
            currentIndex++;
            wordsAndPunct[currentIndex] ? wordsAndPunct[currentIndex] += char : wordsAndPunct[currentIndex] = char;
        } else 
        if(spaces.includes(char))
        {
            currentIndex+=(wordsAndPunct[currentIndex])?1:0;
        } else
        {
            wordsAndPunct[currentIndex] ? wordsAndPunct[currentIndex] += char : wordsAndPunct[currentIndex] = char;
        }  
    });
    return wordsAndPunct;
}

function getWordPossibilities(words)
{
    const possibilities = {};
    for(let i = 0; i < words.length - 1; i++)
    {
        if(!possibilities[words[i]])
        {
            possibilities[words[i]] = [];
        }
        possibilities[words[i]].push(words[i+1]);
    }
    return possibilities;
}

function generateWord(wordlist, word=".")
{
    return wordlist[word][(Math.random()*wordlist[word].length)>>0]
}

function generateWords(wordlist, times=1000)
{
    let currentWord;
    let finalText = "";
    for(let i = 0; i < times; i++)
    {
        currentWord = generateWord(wordlist, currentWord);
        finalText += `${currentWord} `;
    }
    return finalText;
}

exports.mapTextToWordsAndPunctuation = mapTextToWordsAndPunctuation;
exports.getWordPossibilities = getWordPossibilities;
exports.generateWords = generateWords;