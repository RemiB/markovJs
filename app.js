const fs = require('fs');
const destDir = "./feedTexte";
const {getWordPossibilities, mapTextToWordsAndPunctuation, generateWord, generateWords} = require("./tools.js");

const dirList = fs.readdirSync(destDir);
const contents = dirList.map((filename) => fs.readFileSync(`${destDir}/${filename}`, 'utf8').toLowerCase()).join("\n\r");

const words = mapTextToWordsAndPunctuation(contents);
const possibilities = getWordPossibilities(words);

const wordsNumber = 1000;

fs.writeFileSync('output.txt', generateWords(possibilities, wordsNumber), 'utf8');
fs.writeFileSync('wordPos.txt', JSON.stringify(possibilities), 'utf8');